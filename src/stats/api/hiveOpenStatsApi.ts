import axios from 'axios'

const ENDPOINT = import.meta.env.VITE_APP_STATS_ENDPOINT || 'https://stats.hivehub.dev'

export const fetchData = async (metric: string, limit: number = 100, offset: number = 0): Promise<any> => {
  return (await axios.get(`${ENDPOINT}/${metric}`, { params: { limit, offset } })).data
}

export const getCommunities = async (limit: number = 200, offset: number = 0): Promise<any> => {
  return (await axios.get('https://stats.hivehub.dev/communities', { params: { limit, offset } })).data
}

export const getCommunitiesByDays = async (limit: number = 200, offset: number = 0, days: number = 0): Promise<any> => {
  return (await axios.get('https://stats.hivehub.dev/communities', { params: { limit, offset, days } })).data
}

export const getCommunity = async (c: string): Promise<any> => {
  return (await axios.get('https://stats.hivehub.dev/communities', { params: { c } })).data
}

export const getCommunityByDays = async (c: string, d: number): Promise<any> => {
  return (await axios.get('https://stats.hivehub.dev/communities', { params: { c, d } })).data
}

export const getComments = async (c: string, u: string): Promise<any> => {
  return (await axios.get('https://stats.hivehub.dev/' + c + '/comments/', { params: { c, u } })).data
}

export const getPosts = async (c: string, u: string): Promise<any> => {
  return (await axios.get('https://stats.hivehub.dev/' + c + '/posts/', { params: { c, u } })).data
}

export const getSubscribers = async (c: string, u: string): Promise<any> => {
  return (await axios.get('https://stats.hivehub.dev/' + c + '/subscribers_change/' + u)).data
}
