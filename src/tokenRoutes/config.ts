import { RouteRecordRaw } from 'vue-router'

const name = 'tokenroutes'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/tokenroutes',
    name: 'tokenroutes',
    component: () => import('~/tokenRoutes/pages/Routes.vue'),
    props: true
  },
  {
    path: '/tokenroutes/tokenlist',
    name: 'tokenlist',
    component: () => import('~/tokenRoutes/pages/Tokens.vue'),
    props: true,
    meta: { title: 'Available Tokens' }
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_TOKENROUTES === 'true' ? true : false, // disabled by default
  routes: routes
}
