import axios from 'axios'

const endpoint = 'https://routes.hivehub.dev'

export const fetchTokens = async (): Promise<any> => {
  return (await axios.get(endpoint + '/token_routes/supported_tokens')).data
}

export const fetchRoutes = async (tokenIn: string, tokenOut: string): Promise<any> => {
  return (await axios.get(endpoint + '/token_routes/' + tokenIn + '/' + tokenOut)).data
}
