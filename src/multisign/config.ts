import { RouteRecordRaw } from 'vue-router'

const name = 'multisign'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/multisign',
    name: 'multisign',
    component: () => import('~/multisign/pages/Multisign.vue'),
    meta: { title: 'Create transaction' }
  },
  {
    path: '/multisign/:encoded',
    name: 'sign',
    component: () => import('~/multisign/pages/Multisign.vue'),
    props: true,
    meta: { title: 'Sign transaction' }
  }
]

export const config = {
  name: name,
  enabled: import.meta.env.VITE_APP_ENABLE_MULTISIGN === 'true' ? true : false, // disabled by default
  routes: routes
}
