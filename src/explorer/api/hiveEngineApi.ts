import SSC from '@hive-engine/sscjs'

const SSC_ENDPOINT = import.meta.env.VITE_APP_SSC_ENDPOINT || 'https://api.hive-engine.com'
let ssc = new SSC(`${SSC_ENDPOINT}/rpc`)

export const getTransaction = async (trxId: string): Promise<any> => {
  return await ssc.getTransactionInfo(trxId)
}

export const getWitnesses = async (): Promise<any[]> => {
  return await ssc.find('witnesses', 'witnesses', {}, 1000, 0, [{ index: 'approvalWeight', descending: true }])
}

export const getWitnessParams = async (): Promise<any> => {
  return await ssc.findOne('witnesses', 'params', {})
}

export const updateHiveEngineEndpoint = (endpoint: string) => {
  ssc = new SSC(`${endpoint}/rpc`)
}
