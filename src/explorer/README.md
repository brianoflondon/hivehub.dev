# Explorer

The `explorer` module is a block explorer for the Hive Blockchain. It allow user to query the blockchain for blocks, transactions, accounts, witnesses (block validators) and proposals. It also support transaction validation and enrichment on 2nd layer sidechains and applications.
